<?php

namespace App\Controller;
use App\Entity\Articulo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ArticuloController extends Controller
{   
    /**
     * @Route("/articulo", name="articulo")
     */
    public function index()
    {
    	$articles = $this->getDoctrine()->getRepository(Articulo::class)->findAll();

        return $this->render('articulo/index.html.twig', array("nombre" => "Listado de articulos","articulos" => $articles));
    }


    /**
     * @Route("/articulo/nuevo", name="nuevo_articulo")
     */
    public function nuevo(Request $Request)
    {
    	//$articles = $this->getDoctrine()->getRepository(Articulo::class)->findAll();

    	$articulo = new Articulo();


    	$form = $this->createFormBuilder($articulo)
	        ->add('titulo', TextType::class, array(
	        	'attr' => array(
	        		'class' => 'form-control'
	        	)
	        ))->add('contenido', TextareaType::class, array(
		        'required' => false,
		        'attr' => array(
		        	'class' => 'form-control'
	        	)
	        ))
	        ->add('save', SubmitType::class, array(
	        	'label' => 'Create',
	        	'attr' => array(
	        		'class' => 'btn btn-primary mt-3'
	        	)
	        ))
	        ->getForm();

	        $form->handleRequest($Request);

	        if($form->isSubmitted() && $form->isValid()){

	        	$articulos = $form->getData();

	        	$entityArticulo = $this->getDoctrine()->getManager();
	        	$entityArticulo->persist($articulos);
	        	$entityArticulo->flush();	

	        	return $this->redirectToRoute("articulo");
	        }	


        return $this->render('articulo/nuevo.html.twig', array("form" => $form->createView()));
    }
    
    /**
     * @Route("/articulo/eliminar/{id}")
     * @Method({"GET", "POST"})
     */
    public function eliminar($id)
    {
    	$articles = $this->getDoctrine()->getRepository(Articulo::class)->find($id);

    	$entityArticulo = $this->getDoctrine()->getManager();
    	$entityArticulo->remove($articles);
    	$entityArticulo->flush();

        //return $this->render('articulo/index.html.twig', array("nombre" => "Listado de articulos","articulos" => $articles));

    	$responser = new Response();
    	$responser->send();


        return $this->redirectToRoute("articulo");
    }

}
